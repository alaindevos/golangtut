package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
)

func makeUI() fyne.CanvasObject {
	mylabel1 := widget.NewLabel("Alain")
	mylabel2 := widget.NewLabel("De Vos")
	mylabel3 := widget.NewLabel("Eddy")
	mylabel4 := widget.NewLabel("De Wolf")
	mycontainer := container.New(layout.NewGridLayoutWithColumns(2), mylabel1, mylabel2, mylabel3, mylabel4)
	return mycontainer
}

func main() {
	a := app.New()
	w := a.NewWindow("Widget Binding")

	w.SetContent(makeUI())
	w.ShowAndRun()
}
