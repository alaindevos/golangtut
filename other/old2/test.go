package main

import (
    "bufio"
    "encoding/json"
    "errors"
    "fmt" 
    "io"
    "math" 
    "log" 
    "os"
    "sort" 
    "strings" 
    "strconv" 
    "time"
)

func main() {
    mydefer()
	myformattedprint()
	myconstandvariable()
	mytypes()
	mystring()
	myconversion()
	myif()
	myswitch()
	myfor()
	myscope()
	myfunction()
	mymethod()
	myfirstclass()
	myarray()
	myslice()
	mymap()
	myset()
	mystruct()
	myconstructor()
	myclass()
	myinterface()
	mypointer()
	mynil()
	myfiles()
	myerror()
	myerror2()
	mynewerror()
	mypanic()
	mypanic2()
	goroutine()
	myarg()
	myjson()
	mytuple()
}

func mydefer(){
	//defer
	defer fmt.Println("Done")
    fmt.Println("Start")
}

func myformattedprint() {
	//Formatted print
	fmt.Println("Hello1")
	fmt.Printf("%v", "Hello2\n")
}

func myconstandvariable() {
	//Const & Variable
	const c1 int = 0
	var v1 int = 0
	v2 := 0
	fmt.Println(c1 + v1 + v2)
	var c3 bool = true
	fmt.Println(c3)
}

func mytypes() {
	//Types
	var days float64 = 12.34
	var posi uint64 = 12
	fmt.Println(days + float64(posi))
    //Variables
	fmt.Println("Hello World")
	var age1 int=20
	var age2 int64=20
	var alen1 float64=1.4
	age3 :=40
	var alen2 = .9999
	const pi float64 = 3.1415
	fmt.Println(age1," ",age2," ",alen1," ",age3," ",alen2," ",pi)

    var abool = true && false
    fmt.Println(abool)
	fmt.Printf("%.3f \n",pi)
	//print data type
	fmt.Printf("%T \n",pi)
}

func mystring() {
	//String,rawstring
	var s1 string = "aaa"
	var s2 string = `bbb
ccc
ddd`
	fmt.Println(s1 + s2)
	//Char
	var aaa rune = 'A'
	fmt.Printf("%c", aaa)
	var mystring string = "BBBB"
	var bbb byte = mystring[0]
	fmt.Printf("%c", bbb)
    
    	//String
	sx:="Hello World"
	fmt.Println(strings.Contains(sx,"World"))
	fmt.Println(strings.Index(sx,"World"))
	fmt.Println(strings.Count(sx,"World"))
	fmt.Println(strings.Replace(sx,"World","To You",-1))
	csv:="1,2,3"
	fmt.Println(strings.Split(csv,","))
	words:=[]string{"c","a","b"}
	sort.Strings(words)
	fmt.Println(words)
	ajoin:=strings.Join(words,", ")
	fmt.Println(ajoin)
    var name1="alain"
    var name2=`devos`
    var name2len=len(name2)
    var name3=name1 + name2
    fmt.Println(name2," ",name2len," ",name3)
}

func myconversion() {
	//conversion
	var fl64 float64 = float64(2)
	fmt.Println(fl64)
	var strx string = strconv.Itoa(2)
	var stry = fmt.Sprintf("%v", 2)
	fmt.Println(strx + stry)
	var c2 int
	var e2 error
	c2, e2 = strconv.Atoi("12345")
	fmt.Println(c2)
	if e2 == nil {
		fmt.Println("Error")
	}
	anInt:=2
	aFloat:=3.1
	aString:="20"
	fmt.Println(float64(anInt))
	fmt.Println(int(aFloat))
	newint,_:=strconv.Atoi(aString)
	fmt.Println(newint)
	fmt.Println(int(aFloat))
}

func myif() {
	//If
	if 0 == 1 {
		fmt.Println("0==1")
	} else if 1 == 1 {
		fmt.Println("1==1")
	} else {
		fmt.Println("Error")
	}
    //Conditions
	myage:=18
	if myage==18 {
		fmt.Println("You are 18")
	} else if myage > 18 {
		fmt.Println("You are older than 18")
	} else {
		fmt.Println("You are younger than 18")
			}	
}

func myswitch() {
	//Switch
	var sw = 0
	switch sw {
	case 0:
		fmt.Println("0")
	case 1:
		fmt.Println("1")
	default:
		fmt.Println("Error")
	}
    myage:=17
    switch myage {
		case 18 : fmt.Println("18")
		case 17 : fmt.Println("17")
		default:  fmt.Println("Other")
	}
}

func myfor() {
	//For
	var count int = 5
	for count > 0 {
		fmt.Println(count)
		count = count - 1
	}
    //Loops
	i:=1
	for i<=3 {
		fmt.Println(i)
		// i++
		i=i+1
	}
	
	for i:=1 ; i<4 ; i++ {
		fmt.Println(i)
	}
}

func myscope() {
	//scope
	var scope int = 3
	{
		scope = 5
	}
	fmt.Println(scope)

    //Closure
	numx:=3
	doublenum:=func() int {
		numx *=2
		return numx
	}
	fmt.Println(doublenum())
	

}

func addone(x int) int {
	return x + 1
}

func factorial(num int) int{
	if num==0 {
		return 1
	}
	return num * factorial(num-1)
}

func addnumlist(numlist []float64) float64{
	sum:=0.0
	for _,val :=range numlist{
		sum+=val
	}
	return sum
}

func returntuple(number int)(int,int){
	return number+1,number+2
}

func variadic(args ...int) int {
	sum:=0
	for _,val :=range args{
		sum+=val
	}
	return sum
}


func changeval(x *int){
	*x=2
}

func myfunction() {
	//myfunction
	fmt.Println(addone(1))
	//Recursion
	fmt.Println(factorial(3))
    //myfunctions
	numlist:=[]float64{1,2,3,4,5}
	fmt.Println(addnumlist(numlist))
	num1,num2:=returntuple(5)
	fmt.Println(num1,num2)
	fmt.Println(variadic(1,2,3,4))
}

type myint int

func (x myint) addone() int {
	return int(x) + 1
}
func mymethod() {
	//mymethod
	var x myint = 1
	fmt.Println(x.addone())
}

type myintint func(int) int

func addonebis(x int, f myintint) int {
	return f(x)
}
func myfirstclass() {
	//First-Class
	fmt.Println(addonebis(1, addone))
}

func myarray() {
	//Arrays
	var planets [2]string
	planets[0] = "Mercury"
	planets[1] = "Venus"
	fmt.Println(planets[0])
	var mylen int = len(planets)
	fmt.Println(mylen)
	planetsbis := [2]string{"Mercury", "Venus"}
	fmt.Println(planetsbis[0])
	//Iterating
	for i, planet := range planets {
		fmt.Println(i, planet)
	}
    //Arrays
    var ar1[3] float64
    ar1[0]=0.123
    ar1[1]=1.123
    ar1[2]=2.123
    fmt.Println(ar1[0])
    
    ar2:=[3]float64 {1,2,3}
    fmt.Println(ar2[0])
    
    for i,value :=range ar1 {
		fmt.Println(i,value)
	}

    for _,value :=range ar1 {
		fmt.Println(value)
	}
}

func printSlice(s string, x []int) {
	fmt.Printf("%s len=%d cap=%d %v\n",
		s, len(x), cap(x), x)
}

func myslice() {
	//Slice
	var planetstris []string
	planetstris = append(planetstris, "Mercury")
	planetstris = append(planetstris, "Venus")
	fmt.Println(planetstris[0])
	var planets4 = planetstris[0:2]
	fmt.Println(planets4[1])
	fmt.Println(len(planets4))
	fmt.Println(cap(planets4))
    //Slice=array without size
    sl1 :=[] int {1,2,3,4}
    sl2 :=sl1[2:4] // 2,3
    fmt.Println(sl2[1])
    sl3 :=make([]int,4,8) // len(sl3)=4 ; cap(sl3)=8
    copy(sl3,sl1) // copy values to sl3
	printSlice("sl3",sl3)
	sl3=append(sl3,9,10)
	printSlice("sl3",sl3)
}

func mymap() {
	//Map
	var mymap = make(map[string]int)
	mymap["Earth"] = 15
	mymap["Venus"] = 32
	fmt.Println(mymap["Earth"])
	var temp int
	var myok bool
	temp, myok = mymap["Earth"]
	if myok {
		fmt.Println(temp)
	} else {
		fmt.Println("No planet")
	}
    //Map:key-value pairs
	mapage:=make(map[string] int)
	mapage["Alain"]=49
	mapage["Eddy"]=56
	fmt.Println(len(mapage))
	delete(mapage,"Eddy")
	fmt.Println(mapage["Alain"])

}

func myset() {
	//Set
	var myset = make(map[int]bool)
	myset[2] = true
	myset[4] = true
	fmt.Println(myset[2])
}

type xy struct {
	x int
	y int
}

type Rectangle struct{
	height float64
	width float64
}

func mystruct() {
	//Struct
	var myxy xy
	myxy.x = 0
	myxy.y = 1
	fmt.Println(myxy.x)
	var myxybis = xy{x: 10, y: 20}
	fmt.Println(myxybis.x)
	//Struct
	rect :=Rectangle{height: 10, width: 20}
	fmt.Println(rect.height)
	fmt.Println(rect.area())
}

func returnxy(x int, y int) xy {
	return xy{x, y}
}
func myconstructor() {
	//Constructor
	var myxytris = returnxy(1, 2)
	fmt.Println(myxytris.x)
}

type square struct {
	size float64
}

func (s square) surface() float64 {
	return s.size * s.size
}
func myclass() {
	//Class
	mysquare := square{4}
	fmt.Println(mysquare.surface())
}

type dog struct {
}
type cat struct {
}

func (d dog) talk() {
	fmt.Println("Woef Woef")
}
func (c cat) talk() {
	fmt.Println("Miauw Miauw")
}

type talkinterface interface {
	talk()
}

func letittalk(ti talkinterface) {
	ti.talk()
}

type Circle struct{
	radius float64
}

func (r Rectangle) area() float64{
	return r.width*r.height
}

func (c Circle) area() float64{
	return math.Pi* math.Pow(c.radius,2)
}

type Shape interface{
	area() float64
}

func getarea(shape Shape) float64{
	return shape.area()
}

func myinterface() {
	//Interface
	var ti talkinterface
	ti = dog{}
	ti.talk()
	ti = cat{}
	ti.talk()
	letittalk(ti)

	rect :=Rectangle{height: 10, width: 20}
	fmt.Println(getarea(rect))
	circ:=Circle{4}
	fmt.Println(getarea(circ))
}

func paddone(px *int) {
	*px = *px + 1
}

type anint int

func (p *anint) paddone() {
	var i = int(*p)
	i = i + 1
	*p = anint(i)
}
func mypointer() {
	//Pointers
	var a int = 5
	var pa *int = &a
	fmt.Println(pa)
	fmt.Printf("%T\n", pa)
	fmt.Println(*pa)
	str1 := "Alain"
	str2 := "Eddy"
	var pname *string = &str1
	pname = &str2
	fmt.Println(*pname)

	paddone(pa)
	fmt.Println(*pa)

	var z anint = anint(2)
	var pz *anint = &z
	pz.paddone()
	fmt.Println(int(*pz))
    
	x:=0
	changeval(&x)
	fmt.Println(x)
	fmt.Println(&x) //Address
	yptr:=new(int)
	changeval(yptr)
	fmt.Println(*yptr)
}

type number struct {
	value int
	valid bool
}

func newNumber(v int) number {
	return number{value: v, valid: true}
}
func (n number) printNumber() {
	if !n.valid {
		fmt.Println("Not a number")
	} else {
		fmt.Println(n.value)
	}
}
func mynil() {
	//Nil-alternative
	n1 := newNumber(42)
	n1.printNumber()
	n2 := number{}
	n2.printNumber()
	//Error
}

func myfiles(){
	
	//Files
	filename:="sample.txt"
	file,err:=os.Create(filename)
	if err!=nil {
		log.Fatal(err)
		}
	file.WriteString("This is line1\n")
	file.WriteString("This is line2\n")
	file.Close()
	
	file,err=os.Open(filename)
	scanner:=bufio.NewScanner(file)
	if err!=nil {
		log.Fatal(err)
		}
	for scanner.Scan(){
		aline:=scanner.Text()
		fmt.Println("<",aline,">")
	}
	file.Close()
}

func writefile(name string) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()
	_, err = fmt.Fprintln(f, "This is a first text")
	if err != nil {
		return err //defer
	}
	_, err = fmt.Fprintln(f, "This is a seconf text")
	return err //defer
}

func myerror() {
	err := writefile("test.txt")
	if err != nil {
		fmt.Println(err)
	}
}

type safeWriter struct {
	w   io.Writer
	err error
}

func (sw *safeWriter) mywriteln(s string) {
	if sw.err != nil {
		return
	}
	_, sw.err = fmt.Fprintln(sw.w, s)
}

func writefile2(name string) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()
	sw := safeWriter{w: f}
	sw.mywriteln("TestA")
	sw.mywriteln("TestB")
	sw.mywriteln("TestC")
	return sw.err //defer
}
func myerror2() {
	err := writefile2("test2.txt")
	if err != nil {
		fmt.Println(err)
	}
}

var (
	ErrNew = errors.New("A NEW ERROR")
)

func returnerror() error {
	return ErrNew
}

func mynewerror() {
	e := returnerror()
	if e != nil {
		fmt.Printf("An error occured", e)
	}
}

func mypanic() {
	defer func() {
		if e := recover(); e != nil {
			fmt.Println(e)
		}

	}()
	panic("This is my panic")
}

type xxpanic struct{}

func mypanic2() {
	defer func() {
		switch p := recover(); p {
		case nil:
			//
		case xxpanic{}:
			fmt.Println("xxpanic")
		default:
			panic(p)
		}
	}()
	panic(xxpanic{})
}

func writeit() {
	time.Sleep(2 * time.Second)
	fmt.Println("KOEKOEK")
}

func goroutine() {
	go writeit()
	go writeit()
	go writeit()
	go writeit()
	time.Sleep(4 * time.Second)
}

func myarg() {
	fmt.Println(len(os.Args))
	fmt.Println(os.Args[0])
}

type Movie struct {
	Title string
	Year  int
}

var movies = []Movie{
	{Title: "Casablanca", Year: 1942},
	{Title: "Bullitt", Year: 1968},
}

func myjson() {
	data, err := json.Marshal(movies)
	if err != nil {
		fmt.Println("Error JSON")
	} else {
		fmt.Printf("%s\n", data)
	}
}

func getmytuple() (int, int) {
	return 5, 7
}

func mytuple() {
	a, b := getmytuple()
	fmt.Println(a)
	fmt.Println(b)
}
