package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// Open database connection
	db, _ := sql.Open("mysql", "root:root@/mysql")
	defer db.Close()

	// Execute the query
	rows, _ := db.Query("SELECT * FROM Names")

	// Get column names
	columns, _ := rows.Columns()

	// Make a slice for the values
	var values []sql.RawBytes = make([]sql.RawBytes, len(columns))

	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	var scanArgs []interface{} = make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	// Fetch rows
	for rows.Next() {
		// get RawBytes from data & update values
		_ = rows.Scan(scanArgs...)
		// Now do something with the data.
		// Here we just print each column as a string.
		var value string
		for i, col := range values {
			// Here we can check if the value is nil (NULL value)
			value = string(col)
			fmt.Println(columns[i], ": ", value)
		}
	fmt.Println("-----------------------------------")
	}
}
