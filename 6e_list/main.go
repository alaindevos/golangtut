package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/widget"
)

func makeUI() fyne.CanvasObject {
	strings := binding.NewStringList()
	strings.Append("stringa")
	strings.Append("stringb")
	fmt.Println(strings.Length())
	val, _ := strings.GetValue(1)
	fmt.Println(val)
	strings.SetValue(1, "stringc")
	l := widget.NewListWithData(strings,
		func() fyne.CanvasObject {
			return widget.NewLabel("aaa")
		},
		func(item binding.DataItem, obj fyne.CanvasObject) {
			text := obj.(*widget.Label)
			text.Bind(item.(binding.String))
		})
	return l

}

func main() {
	a := app.New()
	w := a.NewWindow("Widget Binding")
	w.Resize(fyne.NewSize(400, 200))
	w.CenterOnScreen()
	w.SetContent(makeUI())
	w.ShowAndRun()
}
