package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	connStr := "postgresql://test:test@127.0.0.1/test?sslmode=disable"
	fmt.Println("Open")
	db, err := sql.Open("postgres", connStr)
	CheckError(err)
	defer db.Close()
	fmt.Println("Select")
	rows, err := db.Query("SELECT * FROM Names")
	CheckError(err)
	defer rows.Close()
	fmt.Println("Print")
	for rows.Next() {
		var firstname string
		var lastname string
		_ = rows.Scan(&firstname, &lastname)
		fmt.Println(firstname, lastname)
	}
}
