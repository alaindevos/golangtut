package main

import (
	"image/color"
	"time"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/widget"
	"fyne.io/fyne/v2/container"
)

func setTime(PL *widget.Label) {
	formatted := time.Now().Format("Time: 03:04:05")
	(*PL).SetText(formatted)
}

func main() {
	APP := app.New()
	WIN := APP.NewWindow("MyClock")
	WIN.Resize(fyne.NewSize(200, 200))

	green := color.NRGBA{R: 0, G: 180, B: 0, A: 255}
	TEXT1:= canvas.NewText("Test1",green)
	blue:= color.NRGBA{R: 0, G: 0, B: 180, A: 255}
	TEXT2:= canvas.NewText("Test2",blue)
	LAB := widget.NewLabel("Fahrenheit")
	setTime(LAB)
	ENT:=widget.NewEntry()
	ENT.SetPlaceHolder("Celcius")
	CONT:=container.NewVBox(TEXT1,TEXT2,LAB,ENT)
	CAN := WIN.Canvas()
	CAN.SetContent(CONT)

	go func() {
		for {
			time.Sleep(time.Second)
			setTime(LAB)
		}
	}()
	WIN.Show()
	APP.Run()
}
