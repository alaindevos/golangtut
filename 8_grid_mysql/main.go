package main

import (
	"database/sql"
	"fmt"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	_ "github.com/go-sql-driver/mysql"
)

func makeUI() fyne.CanvasObject {
	mycontainer := container.New(layout.NewGridLayoutWithColumns(2))
	db, _ := sql.Open("mysql", "root:root@/mysql")
	defer db.Close()
	rows, _ := db.Query("SELECT * FROM Names")
	columns, _ := rows.Columns()
	var values []sql.RawBytes = make([]sql.RawBytes, len(columns))
	var scanArgs []interface{} = make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}
	for rows.Next() {
		_ = rows.Scan(scanArgs...)
		var value string
		for i, col := range values {
			value = string(col)
			fmt.Println(columns[i], ": ", value)
			mylabel := widget.NewLabel(value)
			mycontainer.Add(mylabel)
		}
		fmt.Println("-----------------------------------")
	}
	return mycontainer
}
func main() {
	a := app.New()
	w := a.NewWindow("Widget Binding")
	w.SetContent(makeUI())
	w.ShowAndRun()
}
